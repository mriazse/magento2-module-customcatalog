<?php

namespace MRiaz\CustomCatalog\Api;

interface ProductRepositoryInterface {
	/**
	 * Returns products with given vpn
	 *
	 * @api
	 *
	 * @param string $vpn
	 *
	 * @return \MRiaz\CustomCatalog\Api\Data\ProductSearchResultInterface
	 */
	public function getByVPN( $vpn );

	/**
	 * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
	 *
	 * @return \MRiaz\CustomCatalog\Api\Data\ProductSearchResultInterface
	 */
	public function getList( $criteria );

	/**
	 * @return \MRiaz\CustomCatalog\Api\Data\ProductInterface
	 */
	public function update();

	/**
	 * @param \MRiaz\CustomCatalog\Api\Data\ProductInterface $product
	 * @return mixed
	 */
	public function save($product);

	/**
	 * @param $id
	 * @return \MRiaz\CustomCatalog\Api\Data\ProductInterface
	 */
	public function getById($id);

}
