<?php
namespace MRiaz\CustomCatalog\Api\Data;

interface UpdateProductResponseInterface {

	/**
	 * @return bool
	 */
	public function getSuccess();

	/**
	 * @param $success
	 * @return mixed
	 */
	public function setSuccess($success);

	/**
	 * @return string
	 */
	public function getError();

	/**
	 * @param $error
	 * @return mixed
	 */
	public function setError($error);
}
