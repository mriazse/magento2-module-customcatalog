<?php
namespace MRiaz\CustomCatalog\Api\Data;

interface ProductSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface {
	/**
	 * @return \MRiaz\CustomCatalog\Api\Data\ProductInterface[]
	 */
	public function getItems();

	/**
	 * @param \MRiaz\CustomCatalog\Api\Data\ProductInterface[] $items
	 *
	 * @return void
	 */
	public function setItems(array $items );
}
