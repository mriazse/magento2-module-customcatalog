<?php
namespace MRiaz\CustomCatalog\Api\Data;

interface ProductInterface {

	/**
	 * Product id
	 *
	 * @return int|null
	 */
	public function getId();

	/**
	 * Set product id
	 *
	 * @param int $id
	 * @return $this
	 */
	public function setId($id);

	/**
	 * Product sku
	 *
	 * @return string
	 */
	public function getSku();

	/**
	 * Set product sku
	 *
	 * @param string $sku
	 * @return $this
	 */
	public function setSku($sku);

	/**
	 * Product $sku
	 *
	 * @return string
	 */
	public function getVpn();

	/**
	 * Set product sku
	 *
	 * @param string $vpn
	 * @return $this
	 */
	public function setVpn($vpn);

	/**
	 * Product Copyright Info
	 *
	 * @return string
	 */
	public function getCopyrightInfo();

	/**
	 * Set product Copyright Info
	 *
	 * @param string $copyrightInfo
	 * @return $this
	 */
	public function setCopyrightInfo($copyrightInfo);
}
