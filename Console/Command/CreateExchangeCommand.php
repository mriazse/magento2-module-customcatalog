<?php

namespace MRiaz\CustomCatalog\Console\Command;

use MRiaz\CustomCatalog\Helper\Messaging\Amqp;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CreateExchangeCommand extends Command {

	/**
	 * @var Amqp
	 */
	private $amqp;

	/**
	 * CreateQueueCommand constructor.
	 *
	 * @param Amqp $amqp
	 * @param null|string $name
	 */
	public function __construct( Amqp $amqp, $name = null ) {
		parent::__construct( $name );
		$this->amqp = $amqp;
	}

	/**
	 * Configures the current command.
	 */
	public function configure() {
		$this->setName( 'mriaz:custom_catalog:exchange:create' );
		$this->setDescription( 'Create exchange in AMQP' );
		$this->addArgument( 'name', InputArgument::REQUIRED, 'Name of the exchange' );
		$this->addOption( 'delayed', null, InputOption::VALUE_NONE, 'Create x-delayed-type exchange' );
	}

	/**
	 * @param InputInterface $input An InputInterface instance
	 * @param OutputInterface $output An OutputInterface instance
	 * @return null|int null or 0 if everything went fine, or an error code
	 */
	public function execute( InputInterface $input, OutputInterface $output ) {
		$this->amqp->createExchange( $input->getArgument( 'name' ), $input->hasOption( 'delayed' ) );
		$output->writeln( sprintf( 'Exchange <comment>%s</comment> created.', $input->getArgument( 'name' ) ) );

		return 0;
	}
}
