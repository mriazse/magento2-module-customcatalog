<?php

namespace MRiaz\CustomCatalog\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use MRiaz\CustomCatalog\Helper\Messaging\Amqp;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class ConsumerCommandAbstract extends Command {

	/**
	 * @var State
	 */
	protected $appState;
	/**
	 * @var Amqp
	 */
	protected $_amqp;

	/**
	 * @var string
	 */
	protected $_exchangeId;
	/**
	 * @var string
	 */
	protected $_errorExchangeId;
	/**
	 * Process limit
	 */
	protected $_processLimit = - 1;

	/**
	 * CreateQueueCommand constructor.
	 *
	 * @param State $state
	 * @param Amqp $amqp
	 * @param null|string $name
	 * @internal param ResourceConnection $resource
	 */
	public function __construct( State $state, Amqp $amqp, $name = null ) {
		parent::__construct( $name );

		$this->_amqp = $amqp;
		$this->appState = $state;
	}

	/**
	 * @param InputInterface $input An InputInterface instance
	 * @param OutputInterface $output An OutputInterface instance
	 *
	 * @return int|null null or 0 if everything went fine, or an error code
	 *
	 * @throws \Exception
	 */
	public function execute( InputInterface $input, OutputInterface $output ) {
		if ( null === $this->_exchangeId ) {
			throw new \Exception( "Please fill the property \$_exchangeId in your Command." );
		}

		$connection = $this->_amqp->getConnection();
		$channel = $connection->channel();
		$queueName = $this->_exchangeId;
		$channel->queue_bind( $queueName, $this->_exchangeId );
		$callback = [ $this, 'callback' ];
		$channel->basic_qos( null, 1, null );

		$tag = $channel->basic_consume( $queueName, '', false, false, false, false,
			function ( AMQPMessage $message ) use ( $input, $output, $callback ) {
				$response = Amqp::REJECT;

				try {
					$response = call_user_func( $callback, $input, $output, $message );
				} catch ( \Exception $e ) {
					if ( null !== $this->_errorExchangeId ) {
						if ( $output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE ) {
							$output->writeln( (string) $e );
						}

						$this->_amqp->sendMessage( $this->_errorExchangeId, [
							'class' => get_class( $this ),
							'exception_serialized' => serialize( $e ),
							'message' => json_decode( $message->getBody(), true ),
						] );
					}
				}
				if ( $response === null || $response === true || (int) $response & Amqp::ACK === Amqp::ACK ) {
					$message->delivery_info['channel']->basic_ack( $message->delivery_info['delivery_tag'] );
				} else {
					$requeue = (bool) $response;
					$message->delivery_info['channel']->basic_reject( $message->delivery_info['delivery_tag'],
						$requeue );
				}
			} );

		register_shutdown_function(
			function ( Amqp $amqp, AMQPChannel $channel ) {
				$channel->close();
				$amqp->closeConnection();
			},
			$this->_amqp,
			$channel
		);

		$output->writeln( sprintf( "<comment>Listening on exchange</comment> <info>%s</info> <comment>with tag</comment> <info>%s</info>\n",
			$this->_exchangeId, $tag ) );
		// Loop as long as the channel has callbacks registered
		$processed = 0;
		while ( count( $channel->callbacks ) ) {
			$channel->wait();
			if ( ++ $processed === $this->_processLimit ) {
				break;
			}
		}

		return 0;
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @param AMQPMessage $message
	 *
	 * @return
	 */
	abstract public function callback( InputInterface $input, OutputInterface $output, AMQPMessage $message );

	/**
	 * Init the Application's area (use AREA_* constants)
	 *
	 * @param string $areaCode
	 */
	protected function _initArea( $areaCode = Area::AREA_ADMINHTML ) {
		$this->appState->setAreaCode( $areaCode );
	}
}
