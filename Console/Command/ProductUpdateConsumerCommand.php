<?php

namespace MRiaz\CustomCatalog\Console\Command;

use Magento\Framework\Exception\NoSuchEntityException;
use MRiaz\CustomCatalog\Api\ProductRepositoryInterface;
use PhpAmqpLib\Message\AMQPMessage;
use MRiaz\CustomCatalog\Helper\Messaging\Amqp;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use MRiaz\CustomCatalog\Model\ProductFactory;

class ProductUpdateConsumerCommand extends ConsumerCommandAbstract {
	/**
	 * @var string
	 */
	protected $_exchangeId = 'product-update';

	/** @var ProductFactory $objectFactory */
	protected $objectFactory;

	public function __construct(
		State $state,
		Amqp $amqp,
		ProductFactory $objectFactory,
		$name = null
	) {
		parent::__construct( $state, $amqp, $name );
		$this->objectFactory = $objectFactory;
	}

	public function configure() {
		$this->setName( 'mriaz:custom_catalog:consumer:product_update' );
		$this->setDescription( 'Sample consumer. Consumes on queue name "consume-me". It displays the message\'s body.' );
	}


	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @param AMQPMessage $message
	 *
	 * @throws \Exception
	 */
	public function callback( InputInterface $input, OutputInterface $output, AMQPMessage $message ) {
		$output->writeln( $message->getBody() );

		$json = json_decode($message->getBody(), true);
		$response = [];

		try {
			$storeId = (int)$json['store_id'];
			$objectInstance = $this->objectFactory->create();
			$objectInstance->load($json['entity_id']);

			$objectInstance->addData($json);
			$product = $objectInstance->save();
			$response['success'] = true;
			$response['entity'] = $product->toArray();
		} catch (NoSuchEntityException $nse) {
			$response['success'] = false;
			$response['message'] = $nse->getMessage();
		}  catch (\Exception $e) {
			$response['success'] = false;
			$response['message'] = $e->getMessage();
		}

		if ( $message->get( 'reply_to' ) ) {
			$this->_amqp->sendMessage( $message->get( 'reply_to' ), $response, [
				'correlation_id' => $message->get( 'correlation_id' )
			], Amqp::MODE_RPC_CALLBACK );
		}
	}
}
