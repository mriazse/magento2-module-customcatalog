<?php
/**
 * ProductSetup
 *
 * @copyright Copyright © 2018 MRiaz. All rights reserved.
 * @author    muhammadriaz.bcs@gmail.com
 */

namespace MRiaz\CustomCatalog\Setup;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;

/**
 * @codeCoverageIgnore
 */
class ProductSetup extends EavSetup
{
    /**
     * Entity type for Product EAV attributes
     */
    const ENTITY_TYPE_CODE = 'mriaz_product';

    /**
     * Retrieve Entity Attributes
     *
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function getAttributes()
    {
        $attributes = [];

		$attributes['sku'] = [
			'type' => 'static',
			'label' => 'SKU',
			'input' => 'text',
			'required' => true,
			'sort_order' => 13,
			'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
			'group' => 'General'
		];

		$attributes['vpn'] = [
			'type' => 'static',
			'label' => 'VPN',
			'input' => 'text',
			'required' => true,
			'sort_order' => 12,
			'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
			'group' => 'General'
		];

		$attributes['copyright_info'] = [
			'type' => 'text',
			'label' => 'Copyright Info',
			'input' => 'textarea',
			'required' => false,
			'sort_order' => 11,
			'global' => ScopedAttributeInterface::SCOPE_STORE,
			'group' => 'General'
		];

        return $attributes;
    }

    /**
     * Retrieve default entities: product
     *
     * @return array
     */
    public function getDefaultEntities()
    {
        $entities = [
            self::ENTITY_TYPE_CODE => [
                'entity_model' => 'MRiaz\CustomCatalog\Model\ResourceModel\Product',
                'attribute_model' => 'MRiaz\CustomCatalog\Model\ResourceModel\Eav\Attribute',
                'table' => self::ENTITY_TYPE_CODE . '_entity',
                'increment_model' => null,
                'additional_attribute_table' => self::ENTITY_TYPE_CODE . '_eav_attribute',
                'entity_attribute_collection' => 'MRiaz\CustomCatalog\Model\ResourceModel\Attribute\Collection',
                'attributes' => $this->getAttributes()
            ]
        ];

        return $entities;
    }
}
