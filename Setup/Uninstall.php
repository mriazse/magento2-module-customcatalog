<?php

/**
 * Uninstall.php
 *
 * @copyright Copyright © 2018 MRiaz. All rights reserved.
 * @author    muhammadriaz.bcs@gmail.com
 */
namespace MRiaz\CustomCatalog\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class Uninstall implements UninstallInterface
{
    /**
     * @var array
     */
    protected $tablesToUninstall = [
        ProductSetup::ENTITY_TYPE_CODE . '_entity',
        ProductSetup::ENTITY_TYPE_CODE . '_eav_attribute',
        ProductSetup::ENTITY_TYPE_CODE . '_entity_datetime',
        ProductSetup::ENTITY_TYPE_CODE . '_entity_decimal',
        ProductSetup::ENTITY_TYPE_CODE . '_entity_int',
        ProductSetup::ENTITY_TYPE_CODE . '_entity_text',
        ProductSetup::ENTITY_TYPE_CODE . '_entity_varchar'
    ];

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context) //@codingStandardsIgnoreLine
    {
        $setup->startSetup();

        foreach ($this->tablesToUninstall as $table) {
            if ($setup->tableExists($table)) {
                $setup->getConnection()->dropTable($setup->getTable($table));
            }
        }

        $setup->endSetup();
    }
}
