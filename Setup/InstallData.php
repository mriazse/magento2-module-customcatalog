<?php
/**
 * InstallData
 *
 * @copyright Copyright © 2018 MRiaz. All rights reserved.
 * @author    muhammadriaz.bcs@gmail.com
 */

namespace MRiaz\CustomCatalog\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Product setup factory
     *
     * @var ProductSetupFactory
     */
    protected $productSetupFactory;

    /**
     * Init
     *
     * @param ProductSetupFactory $productSetupFactory
     */
    public function __construct(ProductSetupFactory $productSetupFactory)
    {
        $this->productSetupFactory = $productSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) //@codingStandardsIgnoreLine
    {
        /** @var ProductSetup $productSetup */
        $productSetup = $this->productSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $productSetup->installEntities();
        $entities = $productSetup->getDefaultEntities();
        foreach ($entities as $entityName => $entity) {
            $productSetup->addEntityType($entityName, $entity);
        }

        $setup->endSetup();
    }
}
