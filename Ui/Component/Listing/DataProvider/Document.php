<?php
/**
 * Document
 *
 * @copyright Copyright © 2018 MRiaz. All rights reserved.
 * @author    muhammadriaz.bcs@gmail.com
 */

namespace MRiaz\CustomCatalog\Ui\Component\Listing\DataProvider;

class Document extends \Magento\Framework\View\Element\UiComponent\DataProvider\Document
{
    protected $_idFieldName = 'entity_id';

    public function getIdFieldName()
    {
        return $this->_idFieldName;
    }
}
