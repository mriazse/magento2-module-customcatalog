<?php
namespace MRiaz\CustomCatalog\Model;

use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use MRiaz\CustomCatalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use MRiaz\CustomCatalog\Helper\Messaging\Rpc;
use MRiaz\CustomCatalog\Model\ResourceModel\Product\CollectionFactory;
use MRiaz\CustomCatalog\Api\Data\ProductSearchResultInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class ProductRepository implements ProductRepositoryInterface {
	protected $objectFactory;
	protected $collectionFactory;
	protected $searchResultsFactory;
	protected $searchCriteriaBuilderFactory;

	/**
	 * @var CollectionProcessorInterface
	 */
	private $collectionProcessor;

	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;

	protected $rpc;

	public function __construct(
		ProductFactory $objectFactory,
		CollectionFactory $collectionFactory,
		ProductSearchResultInterfaceFactory $searchResultsFactory,
		SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
		CollectionProcessorInterface $collectionProcessor,
		Rpc $rpc,
		\Magento\Store\Model\StoreManagerInterface $storeManager
	) {
		$this->objectFactory = $objectFactory;
		$this->collectionFactory = $collectionFactory;
		$this->searchResultsFactory = $searchResultsFactory;
		$this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
		$this->collectionProcessor = $collectionProcessor;
		$this->rpc = $rpc;
		$this->storeManager = $storeManager;
	}

	/**
	 * @inheritdoc
	 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
	 * @SuppressWarnings(PHPMD.NPathComplexity)
	 */
	public function getList( $searchCriteria ) {
		/** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
		$collection = $this->collectionFactory->create();

		$collection->addAttributeToSelect('*');

		$this->collectionProcessor->process($searchCriteria, $collection);

		$collection->load();

		$searchResult = $this->searchResultsFactory->create();
		$searchResult->setSearchCriteria($searchCriteria);
		$searchResult->setItems($collection->getItems());
		$searchResult->setTotalCount($collection->getSize());

		return $searchResult;
	}

	/**
	 * @inheritdoc
	 */
	public function getByVPN( $vpn ) {
		$builder = $this->searchCriteriaBuilderFactory->create();

		$builder->addFilter('VPN', $vpn);

		$criteria = $builder->create();

		return $this->getList($criteria);
	}

	/**
	 * @inheritdoc
	 */
	public function getById( $id ) {
		$object = $this->objectFactory->create();
		$storeId = (int)$this->storeManager->getStore()->getId();
		$object->setData('store_id', $storeId);

		$object->load( $id );

		if ( !$object->getId() ) {
			throw new NoSuchEntityException( __( 'Object with id "%1" does not exist.', $id ) );
		}
		return $object;
	}

	/**
	 * @inheritdoc
	 */
	public function update() {
		try {
			$storeId = (int)$this->storeManager->getStore()->getId();
			$json = json_decode( file_get_contents( "php://input" ), true );
			$json['store_id'] = $storeId;
			$json['store'] = $storeId;
			$response = $this->rpc->directRequest( 'product-update', $json);
			$response = json_decode($response, true) ?? [];

			if (!$response['success']) {
				throw new \Exception($response['message']);
			}

			$product = $this->objectFactory->create();
			$product->addData($response['entity']);

			return $product;
		} catch (\Exception $e) {
			throw new CouldNotSaveException( __( $e->getMessage() ) );
		}
	}

	/**
	 * @inheritdoc
	 */
	public function save( $product ) {
		try {
			$product->save();
		} catch ( \Exception $e ) {
			throw new CouldNotSaveException( __( $e->getMessage() ) );
		}

		return $product;
	}
}
