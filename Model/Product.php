<?php

/**
 * Product.php
 *
 * @copyright Copyright © 2018 MRiaz. All rights reserved.
 * @author    muhammadriaz.bcs@gmail.com
 */

namespace MRiaz\CustomCatalog\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Product extends AbstractModel implements IdentityInterface, \MRiaz\CustomCatalog\Api\Data\ProductInterface {
	/**
	 * CMS page cache tag
	 */
	const CACHE_TAG = 'mriaz_customcatalog_product';

	/**
	 * @var string
	 */
	protected $_cacheTag = 'mriaz_customcatalog_product';

	/**
	 * Prefix of model events names
	 *
	 * @var string
	 */
	protected $_eventPrefix = 'mriaz_customcatalog_product';

	/**
	 * Product Store Id
	 */
	const STORE_ID = 'store_id';

	/**
	 * @var string
	 */
	protected $_eventObject = 'product';

	/**
	 * Initialize resource model
	 *
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->_init( 'MRiaz\CustomCatalog\Model\ResourceModel\Product' );
	}


	/**
	 * Get identities
	 *
	 * @return array
	 */
	public function getIdentities() {
		return [ self::CACHE_TAG . '_' . $this->getId() ];
	}

	/**
	 * Identifier getter
	 *
	 * @return int
	 * @since 101.0.0
	 */
	public function getId()
	{
		return $this->_getData('entity_id');
	}

	/**
	 * Set entity Id
	 *
	 * @param int $value
	 * @return $this
	 * @since 101.0.0
	 */
	public function setId($value)
	{
		return $this->setData('entity_id', $value);
	}

	public function getSku() {
		return $this->_getData('sku');
	}

	public function setSku( $sku ) {
		return $this->setData('sku', $sku);
	}

	public function getVpn() {
		return $this->_getData('vpn');
	}

	public function setVpn( $vpn ) {
		return $this->setData('vpn', $vpn);
	}

	public function getCopyrightInfo() {
		return $this->_getData('copyright_info');
	}

	public function setCopyrightInfo( $copyrightInfo ) {
		return $this->setData('copyright_info', $copyrightInfo);
	}


	/**
	 * Save from collection data
	 *
	 * @param array $data
	 * @return $this|bool
	 */
	public function saveCollection( array $data ) {
		if ( isset( $data[$this->getId()] ) ) {
			$this->addData( $data[$this->getId()] );
			$this->getResource()->save( $this );
		}

		return $this;
	}

	/**
	 * Set product store id
	 *
	 * @param int $storeId
	 * @return $this
	 */
	public function setStoreId($storeId)
	{
		return $this->setData(self::STORE_ID, $storeId);
	}

	/**
	 * Retrieve Store Id
	 *
	 * @return int
	 */
	public function getStoreId() {
		if ( $this->hasData( self::STORE_ID ) ) {
			return $this->getData( self::STORE_ID );
		}

		return 0;
	}
}



