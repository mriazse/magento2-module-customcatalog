<?php
namespace MRiaz\CustomCatalog\Model;

/**
 * @api
 */
class UpdateProductResponse implements \MRiaz\CustomCatalog\Api\Data\UpdateProductResponseInterface {
	protected $success;
	protected $error;

	/**
	 * @inheritdoc
	 */
	public function getSuccess() {
		return $this->success;
	}

	/**
	 * @inheritdoc
	 */
	public function getError() {
		return $this->error;
	}

	public function setSuccess( $success ) {
		$this->success = $success;
	}

	public function setError( $error ) {
		$this->error = $error;
	}

}
